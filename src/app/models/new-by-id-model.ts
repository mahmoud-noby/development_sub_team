
    export class NewsSource {
        id: number;
        name: string;
        link: string;
        avatar: string;
    }

    export class NewsTag {
        id: string;
        name: string;
    }

    export class NewsSource2 {
        id: number;
        name: string;
        link: string;
        avatar: string;
    }

    export class RelatedArticle {
        id: string;
        cover: string;
        title: string;
        content_brief: string;
        created_at: number;
        views_count: number;
        likes_count: number;
        comments_count: number;
        is_liked?: any;
        saved_link_id?: any;
        share_count: number;
        share_url: string;
        news_source: NewsSource2;
    }

    export class SpecialTag {
        id?: any;
        name?: any;
        icon?: any;
        color?: any;
    }

    export class News {
        id: string;
        cover: string;
        title: string;
        content_brief: string;
        created_at: number;
        views_count: number;
        likes_count: number;
        comments_count: number;
        is_liked?: any;
        saved_link_id?: any;
        share_count: number;
        share_url: string;
        news_source: NewsSource;
        comments: any[];
        news_tags: NewsTag[];
        related_articles: RelatedArticle[];
        special_tag: SpecialTag;
    }

    export class NewsById {
        news: News;
    }


