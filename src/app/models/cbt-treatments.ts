export class Treatment {
    constructor(public id?: number, public name?: string) {
    }
}

export class TreatmentsGroup {
    constructor(public id?: number, public name?: string, public image?: string, public treatments?: Treatment[]) {
    }
}

export class CbtTreatments {
    constructor(public treatments_groups?: TreatmentsGroup[]) {
    }
}
