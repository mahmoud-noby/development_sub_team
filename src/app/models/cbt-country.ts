export interface CrossBorderTreatmentCountry {
    country_code: string;
    country: string;
    country_image: string;
    country_flag: string;
}

export interface CbtCountry {
    cross_border_treatment_countries: CrossBorderTreatmentCountry[];
}
