export interface NewsTag {
    id: string;
    name: string;
}

export interface NewsTags {
    news_tags: NewsTag[];
}
