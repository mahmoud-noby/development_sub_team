export interface NewsSource {
    id: number;
    name: string;
    link: string;
    avatar: string;
}

export interface News {
    id: string;
    cover: string;
    title: string;
    content_brief: string;
    body: string;
    created_at: number;
    views_count: number;
    likes_count: number;
    comments_count: number;
    is_liked?: any;
    saved_link_id?: any;
    share_count: number;
    share_url: string;
    news_source: NewsSource;
}

export interface Meta {
    total_pages: number;
}

export interface HomeNews {
    news: News[];
    meta: Meta;
}
