import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CbtTreatments } from 'src/app/models/cbt-treatments';
import { CbtCountry } from 'src/app/models/cbt-country';

@Injectable({
  providedIn: 'root'
})
export class CbtService {
  cbtUrl = 'https://admin.balsamee.com/api/v5/treatments_groups?lang=en' ;
  countriesUrl = 'https://admin.balsamee.com/api/v5/cross_border_treatments/countries';


  constructor( private http: HttpClient) {}

  getTreatmentsGroups(): Observable<CbtTreatments> {
    return this.http.get<CbtTreatments>(this.cbtUrl);
  }
  getTreatments(): Observable<CbtTreatments> {
    return this.http.get<CbtTreatments>(this.cbtUrl);
  }


  getCountries(): Observable<CbtCountry> {
    return this.http.get<CbtCountry>(this.countriesUrl);
  }
}
