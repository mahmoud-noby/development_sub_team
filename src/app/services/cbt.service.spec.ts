import { TestBed } from '@angular/core/testing';

import { CbtService } from './cbt.service';

describe('CbtService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CbtService = TestBed.get(CbtService);
    expect(service).toBeTruthy();
  });
});
