import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HomeNews } from 'src/app/models/news-model';
import { NewsTags } from 'src/app/models/news-tags-model';
import { NewsById } from 'src/app/models/new-by-id-model';

@Injectable({
  providedIn: 'root'
})
export class NewsHealthTipsService {
  news = 'https://admin.balsamee.com/api/v5/news';
  tags = 'https://admin.balsamee.com/api/v5/news_feeds';
  newsTagsUrl = 'https://admin.balsamee.com/api/v5/news_tags';

  constructor( private http: HttpClient) { }
  getNews(page, perPage, lang, news_sort): Observable<HomeNews> {
    return this.http.get<HomeNews>
    (this.news + `?page=${page}&per_page=${perPage}&lang=${lang}&news_sort=${news_sort}`);
  }
  getNewsByTags( newTagsId:string, page, perPage, lang, news_sort): Observable<any> {
    return this.http.get<HomeNews>
    (this.tags + `?page=${page}&per_page=${perPage}&lang=${lang}&news_tag_id=${newTagsId}&news_sort=${news_sort}`);
  }
  getNewById(newId: string, lang) {
    return this.http.get<NewsById>
    (this.news + '/' + newId + `?lang=${lang}`);
  }
  getNewsTags(): Observable<NewsTags> {
    return this.http.get<NewsTags>(this.newsTagsUrl);
   }
}
