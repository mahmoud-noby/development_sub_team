import { TestBed } from '@angular/core/testing';

import { NewsHealthTipsService } from './news-health-tips.service';

describe('NewsHealthTipsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewsHealthTipsService = TestBed.get(NewsHealthTipsService);
    expect(service).toBeTruthy();
  });
});
