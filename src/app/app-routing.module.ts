import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './components/home-page/home-page.component';
import { HealthTipsPageComponent } from './components/health-tips-page/health-tips-page.component';
import { HealthTipsDetailsComponent } from './components/health-tips-details/health-tips-details.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component:HomePageComponent },
  { path: 'health-tips', component: HealthTipsPageComponent },
  { path: 'health-tips/:id', component: HealthTipsDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
