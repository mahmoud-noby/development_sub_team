import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NewsTag } from 'src/app/models/news-tags-model';
import { News } from 'src/app/models/news-model';
import { NewsHealthTipsService } from 'src/app/services/news-health-tips.service';

@Component({
  selector: 'app-health-tips-page',
  templateUrl: './health-tips-page.component.html',
  styleUrls: ['./health-tips-page.component.css']
})
export class HealthTipsPageComponent implements OnInit {
  news: News[];
  morenews: News[];
  newsbyid: News[] = [];
  healthTipstags: NewsTag[];
  totalPages: number;
  page: number = 1;
  perPage: number = 6;
  lang: string = 'en';
  isliked: boolean = false;
  isLoading: boolean = false

  news_sort: string = 'chronological';
  constructor(private newsHealthtipsService: NewsHealthTipsService, private router: Router) { }
  ngOnInit() {
    this.getNews();
    this.getTags();
  }
  getNews() {
    this.isLoading = true;

    this.newsHealthtipsService.getNews(this.page, this.perPage, this.lang, this.news_sort).subscribe(newsresponse => {
      this.news = newsresponse.news;
      this.isLoading = false;

      console.log(this.news);
    });
  }


  loadMoreNews() {
    this.isLoading = true;
    if (this.page !== this.totalPages) {
      this.perPage += 6;
      this.getNews();
    }
  }

  getTags() {
    this.newsHealthtipsService.getNewsTags().subscribe(tags => {
      this.healthTipstags = tags.news_tags;
    });
  }

  newsFeedByTag(newTagId: string) {
    this.isLoading = true;

    this.newsbyid = [];
    this.newsHealthtipsService.getNewsByTags(newTagId, this.page, this.perPage, this.lang, this.news_sort).subscribe(feednews => {
      feednews.news_feeds.map(item => {
        this.newsbyid.push(item.news);
      });
      this.news = this.newsbyid;
      this.isLoading = false;
    });
  }

  onSelect(card: News) {
    this.router.navigate(['/health-tips', card.id]);
  }

}
