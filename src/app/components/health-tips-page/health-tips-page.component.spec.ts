import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthTipsPageComponent } from './health-tips-page.component';

describe('HealthTipsPageComponent', () => {
  let component: HealthTipsPageComponent;
  let fixture: ComponentFixture<HealthTipsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthTipsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthTipsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
