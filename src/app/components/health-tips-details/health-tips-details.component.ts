import { NewsHealthTipsService } from './../../services/news-health-tips.service';
import { News } from 'src/app/models/news-model';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NewsById } from 'src/app/models/new-by-id-model';

@Component({
  selector: 'app-health-tips-details',
  templateUrl: './health-tips-details.component.html',
  styleUrls: ['./health-tips-details.component.css']
})
export class HealthTipsDetailsComponent implements OnInit {
  lang = 'en';
  new: NewsById;
  isLoading: boolean;
  constructor( private activatedRoute: ActivatedRoute, private healthTipsService: NewsHealthTipsService) { }

  ngOnInit()  {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      this.isLoading = true;

      if (!paramMap.has('id')) {
        return;
      }
      const Id = paramMap.get('id');
      this.healthTipsService.getNewById(Id , this.lang).subscribe ( newData => {
        this.new = newData;
        console.log(newData);
        this.isLoading = false;

      });
    });
  }

}
