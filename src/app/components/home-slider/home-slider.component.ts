import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

export interface ISlider {
  sliderId: number;
  imageUrl: string;
}

@Component({
  selector: 'app-home-slider',
  templateUrl: './home-slider.component.html',
  styleUrls: ['./home-slider.component.css']
})
export class HomeSliderComponent implements OnInit {
  slidesStore: ISlider[] = [
    {
    sliderId: 1,
    imageUrl: './assets/assets/balsamee_banner.jpg'
    },
    {
    sliderId: 2,
    imageUrl: './assets/assets/balsamee-cbt-slider-english.jpg'
    },
    {
    sliderId: 3,
    imageUrl: './assets/assets/nearby-services.jpg'
    },
  ];

  customOptions: OwlOptions = {
    autoplay: true,
    loop: true,
    mouseDrag: false,
    touchDrag: true,
    pullDrag: false,
    dots: true,
    navSpeed: 700,
    navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>',
              '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    items: 1,
    nav: true,
    responsive: {
      0: {
        dots: false,
        nav: false
      },
      400: {
        dots: false,
        nav: false
      },
      740: {
        dots: false,
        nav: true
      },
      940: {
        dots: true,
        nav: true
      }
    },
  };
  constructor() { }

  ngOnInit() {
  }

}
